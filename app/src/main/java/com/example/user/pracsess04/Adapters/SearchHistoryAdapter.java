package com.example.user.pracsess04.Adapters;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.user.pracsess04.MovieList;
import com.example.user.pracsess04.R;
import com.example.user.pracsess04.models.OMDBAPI;

import java.util.List;

/**
 * Created by user on 18-May-17.
 */

public class SearchHistoryAdapter extends BaseAdapter {
    List<MovieList> movies;
    Context mcontext;

    public SearchHistoryAdapter(List<MovieList> movies, Context mcontext) {
        this.movies = movies;
        this.mcontext = mcontext;
    }

    @Override
    public int getCount() {
        return movies.size();
    }

    @Override
    public Object getItem(int position) {
        return movies.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView= LayoutInflater.from(mcontext).inflate(R.layout.movie_list_items,parent,false);
        ImageView movie_img=(ImageView) rowView.findViewById(R.id.movie_img);
        Glide.with(mcontext).load(movies.get(position).getMovieUrl()).into(movie_img);
        TextView movie_title=(TextView) rowView.findViewById(R.id.movie_title);
        movie_title.setText(movies.get(position).getMovieTitle());
        TextView movie_content= (TextView) rowView.findViewById(R.id.movie_content);
        movie_content.setText(movies.get(position).getMovieContent());
        return rowView;
    }
}
