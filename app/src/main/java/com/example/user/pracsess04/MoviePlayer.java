package com.example.user.pracsess04;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class MoviePlayer extends AppCompatActivity {
VideoView myVideo;
    String videoUrl;
    BroadcastReceiver myCallReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_player);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 100);

        }

videoUrl="http://hw5.asset.aparat.com/aparat-video/d672086d1ee391a3cada84830dd3688b7143354-180p__51943.mp4";
        myVideo =(VideoView) findViewById(R.id.myVideo);
        myVideo.setMediaController(new MediaController(this));
        myVideo.setVideoURI(Uri.parse(videoUrl));
        myVideo.start();

        myCallReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(myVideo.isPlaying()){
                    myVideo.pause();
                }
            }
        };
        IntentFilter myCallFilter = new IntentFilter("android.intent.action.PHONE_STATE");
        registerReceiver(myCallReceiver,myCallFilter);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(myCallReceiver);
    }
}
