package com.example.user.pracsess04;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.user.pracsess04.models.OMDBAPI;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.util.List;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {
    EditText search_title;
    Button btn_search;
    Button btn_showHistory;
    ImageView img_movie;
    TextView title_movie;
    TextView content_movie;
    String _Url;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        search_title = (EditText) findViewById(R.id.search_title);
        btn_search = (Button) findViewById(R.id.btn_search);
        btn_showHistory = (Button) findViewById(R.id.btn_showHistory);
        img_movie = (ImageView) findViewById(R.id.img_movie);
        title_movie = (TextView) findViewById(R.id.title_movie);
        content_movie = (TextView) findViewById(R.id.content_movie);

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _Url = "http://www.omdbapi.com/?t=" + search_title.getText().toString();
                getMoviesByUrlUsingAsyncHttp(_Url);
                search_title.setText("");


            }
        });
        btn_showHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                List<MovieList> model = MovieList.listAll(MovieList.class);
//                for (MovieList movie: model
//                     ) {
//                    Toast.makeText(MainActivity.this,movie.getMovieTitle(),Toast.LENGTH_LONG).show();
//                }
                Intent historyActivityIntent = new Intent(MainActivity.this, HistoryActivity.class);

                startActivity(historyActivityIntent);

                finish();

            }
        });

    }

    public void getMoviesByUrlUsingAsyncHttp(String _url) {

        AsyncHttpClient httpClient = new AsyncHttpClient();
        httpClient.get(_url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.d("getMovieAsyncFailure", "Error occured: " + throwable.toString());
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                setMovieDetailsByGson(responseString);
            }
        });

    }

    public void setMovieDetailsByGson(String _webServiceResponse) {
        Gson gson = new Gson();
        OMDBAPI omdbapi = gson.fromJson(_webServiceResponse, OMDBAPI.class);
        Glide.with(MainActivity.this).load(omdbapi.getPoster()).into(img_movie);
        title_movie.setText(omdbapi.getTitle());
        content_movie.setText(omdbapi.getPlot());
        MovieList movieList = new MovieList();
        movieList.setMovieContent(omdbapi.getPlot());
        movieList.setMovieTitle(omdbapi.getTitle());
        movieList.setMovieUrl(omdbapi.getPoster());
        movieList.save();
    }
}
