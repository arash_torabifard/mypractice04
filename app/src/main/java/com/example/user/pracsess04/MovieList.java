package com.example.user.pracsess04;

import com.orm.SugarRecord;

/**
 * Created by user on 18-May-17.
 */

public class MovieList extends SugarRecord {

    String movieUrl;
    String movieTitle;
    String movieContent;

    public MovieList(String movieUrl, String movieTitle, String movieContent) {
        this.movieUrl = movieUrl;
        this.movieTitle = movieTitle;
        this.movieContent = movieContent;
    }

    public MovieList() {
    }

    public String getMovieUrl() {
        return movieUrl;
    }

    public void setMovieUrl(String movieUrl) {
        this.movieUrl = movieUrl;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public String getMovieContent() {
        return movieContent;
    }

    public void setMovieContent(String movieContent) {
        this.movieContent = movieContent;
    }
}
