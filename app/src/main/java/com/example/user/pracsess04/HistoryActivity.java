package com.example.user.pracsess04;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.example.user.pracsess04.Adapters.SearchHistoryAdapter;
import com.example.user.pracsess04.R;
import com.example.user.pracsess04.models.OMDBAPI;

import java.util.List;

public class HistoryActivity extends AppCompatActivity {
ListView list_movies;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        List<MovieList> movies= MovieList.listAll(MovieList.class);
        list_movies = (ListView) findViewById(R.id.list_movies);
        SearchHistoryAdapter adapter = new SearchHistoryAdapter(movies,HistoryActivity.this);
        list_movies.setAdapter(adapter);
    }
}
